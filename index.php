<?php

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', true);

// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

// directory separator shortcut
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

$config=__DIR__.'/app/config/main.php';
require(__DIR__.'/../vendor/yii.php');
require(__DIR__.'/app/helpers/helpers.php');

Yii::createWebApplication($config)->run();