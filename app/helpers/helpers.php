<?php

function url($url='', $params=[])
{
	return Yii::app()->createUrl($url, $params);
}

function site_url($url='', $params=[])
{
	return Yii::app()->createAbsoluteUrl($url, $params);
}

function asset($asset='')
{
	// @todo: clean it up
	if (Yii::app()->theme)
	{
		return Yii::app()->theme->baseUrl.'/assets/'.$asset;
	}
	return Yii::app()->request->baseUrl.'/assets/'.$asset;
}

function upload($url='')
{
	return Yii::app()->request->baseUrl."/uploads/".$url;
}

function t($name='', $value)
{
	return Yii::t($name, $value);
}


function e($value='')
{
	return CHtml::encode($value);
}

function params($key='')
{
	return Yii::app()->params[$key];
}

function link_to($label='', $href='', $opts=[])
{
	return CHtml::link($label, $href, $opts);
}

function dd($value='')
{
	printf("<pre>%s</pre>", var_dump($value));die;
}