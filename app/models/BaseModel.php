<?php

class BaseModel extends CActiveRecord {

	public function listOptions()
	{
		return CHtml::listData($this->findAll(), 'id', 'name');
	}

}