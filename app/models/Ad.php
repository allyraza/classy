<?php

/**
 * This is the model class for table "ads".
 *
 * The followings are the available columns in table 'ads':
 * @property integer $id
 * @property integer $user_id
 * @property integer $category_id
 * @property integer $location_id
 * @property string $title
 * @property double $price
 * @property integer $currency_id
 * @property string $description
 * @property string $description_filtered
 * @property string $type
 * @property integer $views
 * @property integer $highlight
 * @property integer $display_phone
 * @property integer $display_email
 * @property integer $status
 * @property string $expiry_date
 * @property string $created_at
 * @property string $updated_at
 */
class Ad extends CActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ads';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('user_id, category_id, location_id, currency_id, views, highlight, display_phone, display_email, status', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('title', 'length', 'max'=>255),
			array('type', 'length', 'max'=>32),
			array('description, description_filtered, expiry_date, created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, category_id, location_id, title, price, currency_id, description, description_filtered, type, views, highlight, display_phone, display_email, status, expiry_date, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'location' => array(self::BELONGS_TO, 'Location', 'location_id'),
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'images' => array(self::HAS_MANY, 'Image', 'imageable_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'user_id' => 'User',
			'category_id' => 'Category',
			'location_id' => 'Location',
			'title' => 'Title',
			'price' => 'Price',
			'currency_id' => 'Currency',
			'description' => 'Description',
			'description_filtered' => 'Description Filtered',
			'type' => 'Type',
			'views' => 'Views',
			'highlight' => 'Highlight',
			'display_phone' => 'Display Phone',
			'display_email' => 'Display Email',
			'status' => 'Status',
			'expiry_date' => 'Expiry Date',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);

		$criteria->compare('user_id',$this->user_id);

		$criteria->compare('category_id',$this->category_id);

		$criteria->compare('location_id',$this->location_id);

		$criteria->compare('title',$this->title,true);

		$criteria->compare('price',$this->price);

		$criteria->compare('currency_id',$this->currency_id);

		$criteria->compare('description',$this->description,true);

		$criteria->compare('description_filtered',$this->description_filtered,true);

		$criteria->compare('type',$this->type,true);

		$criteria->compare('views',$this->views);

		$criteria->compare('highlight',$this->highlight);

		$criteria->compare('display_phone',$this->display_phone);

		$criteria->compare('display_email',$this->display_email);

		$criteria->compare('status',$this->status);

		$criteria->compare('expiry_date',$this->expiry_date,true);

		$criteria->compare('created_at',$this->created_at,true);

		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider('Ad', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return Ad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}

/*

	public function isFreeCode( $code )
	{
		$ret = 0;
		$res = $this->find('code = :code', array(':code' => $code));
		if(!empty($res)){
			$ret = 1;
		}
		return $ret;
	}
	

	public function getAdById( $ad_id )
	{
		$ret = 0;
		$res = $this->findByPk( $ad_id );
		if(!empty($res)){
			$ret = 1;
		}
		return $ret;
	}
	
	
	public function getAdByIdAndCode( $ad_id, $code )
	{
		$ret = 0;
		$res = $this->find('ad_id = :ad_id AND code = :code', array(':ad_id' => $ad_id, ':code' => $code));
		if(!empty($res)){
			$ret = 1;
		}
		return $ret;
	}
	
	public function getAdCount(CDbCriteria $criteria)
	{
		$cache_key_name = 'getAdCount_' . md5(serialize($criteria));
		if(!$ret = Yii::app()->cache->get($cache_key_name)){
			$ret = $this->count($criteria);
			Yii::app()->cache->set($cache_key_name , $ret);
		}
		return $ret;
	}
	
	public function getAdList(CDbCriteria $criteria)
	{
		$cache_key_name = 'getAdList_' . md5(serialize($criteria));
		if(!$ret = Yii::app()->cache->get($cache_key_name)){
			$ret = $this->findAll($criteria);
			Yii::app()->cache->set($cache_key_name , $ret);
		}
		return $ret;	
	}
	
	public function createCriteria($_where_to_check = array())
	{
		//create criteria object
		$criteria = new CDbCriteria();
		
		//init criteria object params
		$params = array();
		
		//filter widget params holder
		$fwp = array();
		if(isset(Yii::app()->params['fwp'])){
			$fwp = Yii::app()->params['fwp'];
		}
		
		//category check
		if($cid = DCUtil::isValidInt($_where_to_check, 'cid')){
			$categoryInfo = Category::model()->findByPk( $cid );
			$inWhereArray = array($cid);

			//check for category childs
			$childs = $categoryInfo->getChilds();
			if(!empty($childs)){
				foreach ($childs as $k){
					$inWhereArray[] = $k['category_id'];
				}
			}
			$criteria->addInCondition('t.category_id', $inWhereArray);
			$fwp['cid'] = $cid;
		} else {
			unset($fwp['cid']);
		}
		
		
		//location check
		if($lid = DCUtil::isValidInt($_where_to_check, 'lid')){
			$criteria->addCondition('t.location_id = :lid');
			$params[':lid'] = $lid;
		}
		
		//search check (keyword or tag)
		if($search_string = DCUtil::isValidString($_where_to_check, 'search_string')){
			$criteria->addCondition('MATCH(ad_title, ad_description, ad_tags) AGAINST (:search_string)');
			$params[':search_string'] = urldecode($search_string);
			$fwp['search_string'] = 1;
		} else {
			unset($fwp['search_string']);
		}
		
		//ad type check
		if($tid = DCUtil::isValidInt($_where_to_check, 'tid')){
			$criteria->addCondition('t.ad_type_id = :ati');
			$params[':ati'] = $tid;
			$fwp['tid']	= $tid;
		} else {
			unset($fwp['tid']);
		}
		
		//price check
		if($price = DCUtil::isValidString($_where_to_check, 'price')){
			if($price_serilizied = base64_decode($price)){
				if($price_array = @unserialize($price_serilizied)){
					if(isset($price_array['from']) && isset($price_array['to']) && is_numeric($price_array['from']) && is_numeric($price_array['to'])){
						$criteria->addCondition('ad_price >= :from AND ad_price <= :to');
						$params[':from'] = $price_array['from'];
						$params[':to'] = $price_array['to'];		
						$fwp['price'] = $price;
					}
				}	
			}
		} else {
			unset($fwp['price']);
		}
		
		//show with pic only
		if(DCUtil::isValidInt($_where_to_check, 'show_with_pic')){
			$criteria->addCondition("t.ad_pic <> '' AND t.ad_pic IS NOT NULL");
			$fwp['show_with_pic'] = 1;
		} else {
			unset($fwp['show_with_pic']);
		}
		
		//show with video only
		if(DCUtil::isValidInt($_where_to_check, 'show_with_video')){
			$criteria->addCondition("t.ad_video <> '' AND t.ad_video IS NOT NULL");
			$fwp['show_with_video'] = 1;
		} else {
			unset($fwp['show_with_video']);
		}
		
		//show with map only
		if(DCUtil::isValidInt($_where_to_check, 'show_with_map')){
			$criteria->addCondition("t.ad_lat <> '' AND t.ad_lat IS NOT NULL");
			$fwp['show_with_map'] = 1;
		} else {
			unset($fwp['show_with_map']);
		}
		
		//show only active
		if(DCUtil::isValidInt($_where_to_check, 'show_active')){
			$criteria->addCondition('t.ad_valid_until >= :today');
			$params[':today'] = date('Y-m-d');	
			$fwp['show_active'] = 1;
		} else {
			unset($fwp['show_active']);
		}
		
		//show only with skype
		if(DCUtil::isValidInt($_where_to_check, 'show_with_skype')){
			$criteria->addCondition("t.ad_skype <> '' AND t.ad_skype IS NOT NULL");
			$fwp['show_with_skype'] = 1;
		} else {
			unset($fwp['show_with_skype']);
		}
		
		//set params to criteria
		$criteria->params = array_merge($criteria->params, $params);
		
		//set filter widget params to registry
		Yii::app()->setParams(array('fwp' => $fwp));
		
		return $criteria;
	}
	
	public function getFilters(CDbCriteria $originalCriteria)
	{
		$ret = array();
		Yii::app()->params['show_additional_filters'] = 0;
		
		//get the command builder
		$commandBuilder = Yii::app()->db->getCommandBuilder();
		
		//get category filter
		//need new original criteria for every filter
		$criteria = clone $originalCriteria;
		$criteria->select = 't.category_id, count(*) AS ad_count, t1.category_title';
		$criteria->group = 't.category_id';
		$criteria->join = 'LEFT JOIN category AS t1 ON t1.category_id = t.category_id';
		$res = $commandBuilder->createFindCommand('ad', $criteria)->queryAll();
		if(!empty($res)){
			$ret['category_filter'] = $res;
		}

		//get type filter
		//need new original criteria for every filter
		$criteria = clone $originalCriteria;
		$criteria->select = 't.ad_type_id, count(DISTINCT t.ad_id) as ad_count, t1.ad_type_name';
		$criteria->group = 't.ad_type_id';
		$criteria->join = 'LEFT JOIN ad_type AS t1 ON t1.ad_type_id = t.ad_type_id';
		$res = $commandBuilder->createFindCommand('ad', $criteria)->queryAll();
		if(!empty($res)){
			$ret['ad_type_filter'] = $res;
		}
		
		//get price filter
		//if selected price range
		if(isset($originalCriteria->params[':from']) && isset($originalCriteria->params[':to'])){
			$data[0] = array('from' => $originalCriteria->params[':from'], 'to' => $originalCriteria->params[':to']);
			$data[0]['ad_count'] = $this->count($originalCriteria);
			$ret['price_filter'] = $data;
		} else {
			//need new original criteria for every filter
			$criteria = clone $originalCriteria;
			$criteria->select = 'max(ad_price) AS max_price';
			$res = $commandBuilder->createFindCommand('ad', $criteria)->queryAll();
			if(!empty($res)){
				$max = $res[0]['max_price'];
				if($max > 0){
					$criteria->select = '';
					$criteria->group = '';
					$criteria->join = '';
					$step = ceil($max / 6);
					$from = 0;
					$to = $step;
					$data = array();
					$criteria->addCondition('ad_price >= :from AND ad_price <= :to');
					for ($i = 0; $i < 6; $i++){
						$data[$i] = array('from' => (int)$from, 'to' => (int)$to);
						$criteria->params = array_merge($criteria->params, array(':from' => $from, ':to' => $to));
						$data[$i]['ad_count'] = $this->count($criteria);
						$from += $step;
						$to += $step;
					}//end of for
					
					if(!empty($data)){
						$ret['price_filter'] = $data;
					}
				}
			}
		}
		
		//get with pic only
		//need new original criteria for every filter
		$criteria = clone $originalCriteria;
		$criteria->addCondition("t.ad_pic <> '' AND t.ad_pic IS NOT NULL");
		
		$res = $this->count($criteria);
		if(!empty($res)){
			$ret['pic_filter'] = $res;
			Yii::app()->params['show_additional_filters'] = 1;
		}

		//get only active
		//need new original criteria for every filter
		$criteria = clone $originalCriteria;
		$criteria->addCondition('t.ad_valid_until >= :today');
		$criteria->params = array_merge($criteria->params, array(':today' => date('Y-m-d')));
		$res = $this->count($criteria);
		if(!empty($res)){
			$ret['active_filter'] = $res;
			Yii::app()->params['show_additional_filters'] = 1;
		}
				
		//get only with skype
		//need new original criteria for every filter
		$criteria = clone $originalCriteria;
		$criteria->addCondition("t.ad_skype <> '' AND t.ad_skype IS NOT NULL");
		$res = $this->count($criteria);
		if(!empty($res)){
			$ret['skype_filter'] = $res;
			Yii::app()->params['show_additional_filters'] = 1;
		}
		
		//get with video only if video is enabled
		//need new original criteria for every filter
		if (ENABLE_VIDEO_LINK_PUBLISH){
			$criteria = clone $originalCriteria;
			$criteria->addCondition("t.ad_video <> '' AND t.ad_video IS NOT NULL");
			$res = $this->count($criteria);
			if(!empty($res)){
				$ret['video_filter'] = $res;
				Yii::app()->params['show_additional_filters'] = 1;
			}
		}
		
		//get with map only if map is enabled
		//need new original criteria for every filter
		if(ENABLE_GOOGLE_MAP){
			$criteria = clone $originalCriteria;
			$criteria->addCondition("t.ad_lat <> '' AND t.ad_lat IS NOT NULL");
			$res = $this->count($criteria);
			if(!empty($res)){
				$ret['map_filter'] = $res;
				Yii::app()->params['show_additional_filters'] = 1;
			}	
		}
		
		return $ret;
	}
	
	public function normalizeTags($tags = '')
	{
		$ret = '';
		if(!empty($tags)){
			$ret = AdTag::string2array($tags);
		}
		return $ret;
	}
	
	public function getSmallPic($_pic = '')
	{
		$ret = Yii::app()->theme->baseUrl . '/front/i/no_photo_small.jpg';
		if(!empty($_pic)){
			if(is_file(PATH_UF_CLASSIFIEDS . 'small-' . $_pic)){
				$ret = SITE_UF_CLASSIFIEDS . 'small-' . $_pic;
			}
		}
		return $ret;
	}

}
*/