<?php

/**
 * FixtureHelper is a command application lets you work with your fixtures outside
 * testing. Currently what it does is just helping you to load you fixtures from your
 * fixture files to your database, without the need to invoke PHPUnit.
 *
 * @author    Sum-Wai Low
 * @author    Fredrik Wollsén <fredrik@neam.se>
 * @link      https://github.com/sumwai/fixtureHelper
 * @copyright Copyright &copy; 2010 Sum-Wai Low, Fredrik Wollsén <fredrik@neam.se>
 */
class FixtureCommand extends CConsoleCommand {

	/**
	 * @var string
	 */
	public $defaultModelPath = 'application.models';

	/**
	 * @var string
	 */
	public $defaultFixturePath = 'application.tests.fixtures';

	private $_fixture;

	function getHelp()
	{
		return <<<EOD
USAGE
  fixture load [--modelPath=folderalias] [--fixturePath=folderalias] --tables=tablename1[,tablename2[,...]]
  
DESCRIPTION
  This command lets you work with your fixtures outside testing
  
PARAMETERS
  * load: Load fixtures into the database
  * --modelPath: The alias to the directory that contains the "model" folder
  Please note that folder "models" should contain the Model class of
  the fixtures to be loaded. Defaults to "application.models". Optional for "load".
  * --fixturePath: The alias to the "fixtures" directory
  * --tables: Name of the tables to be loaded with your defined fixtures. Name
  values are comma separated. Required for "load".  
  
EXAMPLES
  yiic fixture load --modelPath=application.modules.mymodule.models --fixturePath=application.modules.mymodule.tests.fixtures --tables=fruit,transport,country


EOD;
	}

	// public function init() {}

	/**
	 * Loads fixtures into the database tables from fixtures.
	 *
	 * @param string $fixturePath alias to the "fixtures" directory
	 * @param string $tables comma separated value of table names that should be loaded with fixtures,
	 *                       or '*' if all fixtures should be loaded. Also calling actionLoad() without any arguments
	 *                       is possible to load all fixtures.
	 */
	function actionLoad($tables = '*', $modelPath = null, $fixturePath = null, $connectionID = "db")
	{

		if (is_null($modelPath))
			$modelPath = $this->defaultModelPath;

		if (is_null($fixturePath))
			$fixturePath = $this->defaultFixturePath;

		Yii::import($modelPath . '.*');
		$this->fixture->connectionID = $connectionID;
		$this->fixture->basePath = Yii::getPathOfAlias($fixturePath);

		$this->fixture->checkIntegrity(false);
		$tables = $this->resolveTables($tables);

		// var_dump($this->fixture);die;

		foreach ($tables as $table)
		{
			try {
				printf("LOADING: ... %s\n", $table);
				$this->fixture->resetTable($table);
				$this->fixture->loadFixture($table);
			} catch (Exception $e) {
				echo "ERROR: There is a problem working with the table $table. " . "Is it spelled correctly or exist?\n\n";
			}
		}

		$this->fixture->checkIntegrity(true);
		echo "DONE!\n";
	}

	private function resolveTables($tables='*')
	{
		if ($tables === '*')
		{
			$tables = [];
			foreach ($this->fixture->fixtures as $name => $path)
				$tables[] = $name;
			return $tables;
		} 
		return explode(',', $tables);
	}

	public function getFixture()
	{
		Yii::import('system.test.CDbFixtureManager');
		if (!$this->_fixture)
			$this->_fixture = new CDbFixtureManager;
		return $this->_fixture;
	}

}