<?php
return array(
	'title' 			=> 'Home Page Title',
	'description' 		=> 'Home Page Description',
	'keywords' 			=> 'Home Page keywords',
	'Latest Classifieds'	=> 'Latest Classifieds',
	'Classifieds by Category' => 'Classifieds by Category'
);