<?php

class CategoryTest extends WebTestCase
{
	public $fixtures=array(
		'categories'=>'Category',
	);

	public function testShow()
	{
		$this->open('?r=categories/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=categories/create');
	}

	public function testUpdate()
	{
		$this->open('?r=categories/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=categories/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=categories/index');
	}

	public function testAdmin()
	{
		$this->open('?r=categories/admin');
	}
}
