<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Classy',

	// preloading 'log' component
	'preload'=>array('log'),

	'commandMap' => array(
		'migrate' => array(
			'class' => 'system.cli.commands.MigrateCommand',
			'migrationTable' => 'migrations'
		)
	),

	// application components
	'components'=>array(

		// uncomment the following to use a MySQL database
		'db'=>require(__DIR__.DS.'database.php'),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				)
			)
		),

		'fixture'=>array(
			'class'=>'system.test.CDbFixtureManager',
		),

	)
);