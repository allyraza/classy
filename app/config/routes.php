<?php
return array(
	'urlFormat' => 'path',
	'showScriptName' => false,
	'rules' => array(

		// map /name-of-the-ad to /ads/view
		'<location:[\w-]+>/<id:\d+>' => 'ads/location',
		'<category:[\w-]+>' => 'ads/index',
		'<category:[\w-]+>/<location:[\w-]+>' => 'ads/index',
		'<category:[\w-]+>/<location:[\w-]+>/<title:[\w-]+>' => 'ads/view',
		
		'page/<title:[\w-]+>' => 'site/page',

		'admin/login' => 'admin/default/login',
		'admin/logout' => 'admin/default/logout',
		'admin/dashboard' => 'admin/default/index',

		// default routes
		'<controller:\w+>/<id:\d+>'=>'<controller>/view',
		'<controller:\w+>/<id:\d+>/update'=>'<controller>/update',
		
		'<module:\w+>/<controller:\w+>/<id:\d+>'=>'<module>/<controller>/view',
		'<module:\w+>/<controller:\w+>/<id:\d+>/update'=>'<module>/<controller>/update',

		'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
		'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
	)
);