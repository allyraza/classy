<?php
return array(
	
	'basePath' => __DIR__ . DIRECTORY_SEPARATOR . '..',

	// preloading 'log' component
	'preload'=>array('log', 'CInit'),

	// autoloading model and component classes
	'import'=>require(__DIR__.DS.'autoload.php'),

	'modules'=>require(__DIR__.DS.'modules.php'),
	
	// application components
	'components'=>array(

		'clientScript'=>array(
			'class'=>'ext.ExtendedClientScript.ExtendedClientScript',
			'combineCss'=>true,
			'compressCss'=>true,
			'combineJs'=>true,
			'compressJs'=>true,
		),

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		'session'=>array(
			'autoStart' => true
		),

		'cache' =>array(
			'class'=>'system.caching.CFileCache',
		),		
		
		// uncomment the following to enable URLs in path-format
		'urlManager'=>require(__DIR__.DS.'routes.php'),
		
		// uncomment the following to use a MySQL database
		'db'=>require(__DIR__.DS.'database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

		'CInit'=>array(
			'class' => 'CInit'
		),

		'request'=>array(
			'enableCsrfValidation'=>true,
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>require(__DIR__.DS.'params.php')

);