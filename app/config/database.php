<?php
return array(
	'connectionString' => 'mysql:host=localhost;dbname=classy',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => '',
	'charset' => 'utf8',
	'enableParamLogging' => true,
	'schemaCachingDuration' => true
);