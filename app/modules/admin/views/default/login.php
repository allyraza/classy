<?php
$this->pageTitle=Yii::app()->name . ' - ' . t('admin', 'Login');
$this->breadcrumbs=array(
	'Login',
);
?>
<h1><?= t('admin', 'Login') ?></h1>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableAjaxValidation'=>true,
)) ?>

	<div class="row">
		<?= $form->labelEx($model,'username') ?>
		<?= $form->textField($model,'username') ?>
		<?= $form->error($model,'username') ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'password') ?>
		<?= $form->passwordField($model,'password') ?>
		<?= $form->error($model,'password') ?>
	</div>

	<div class="row rememberMe">
		<?= $form->checkBox($model,'rememberMe') ?>
		<?= $form->label($model,'rememberMe') ?>
		<?= $form->error($model,'rememberMe') ?>
	</div>

	<div class="row buttons">
		<?= CHtml::htmlButton(t('admin', 'Login'), ['type' => 'submit']) ?>
	</div>

<?php $this->endWidget() ?>
</div><!-- form -->