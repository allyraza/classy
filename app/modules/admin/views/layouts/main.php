<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="language" content="en">
	
	<title>Admin - <?= e($this->pageTitle) ?></title>

<?php

	Yii::app()->clientScript
		->registerCssFile($this->module->assetsUrl.'/css/base.css')
		->registerCssFile($this->module->assetsUrl.'/css/main.css')
		->registerCssFile($this->module->assetsUrl.'/css/form.css');

?>

</head>
<body class="page <?= $this->id . ' ' . $this->action->id ?>">

<header id="site-header">

	<div id="logo">
		<?= link_to(e(Yii::app()->name), ['/admin/default/index']) ?>
	</div>

	<div id="mainmenu">
	<?php $this->widget('zii.widgets.CMenu', [
		'items'=>array(
			array('label'=>t('admin', 'Dashboard'), 'url'=>['/admin/dashboard']),
			array('label'=>t('admin', 'Ads'), 'url'=>['/admin/ads']),
			array('label'=>t('admin', 'Categories'), 'url'=>['/admin/categories']),
			array('label'=>t('admin', 'Locations'), 'url'=>['/admin/locations']),
			array('label'=>t('admin', 'Pages'), 'url'=>['/admin/pages']),
			array('label'=>t('admin', 'Settings'), 'url'=>['/admin/settings']),
			array('label'=>t('admin', 'Themes'), 'url'=>['/admin/themes'])
		)
	]) ?>
	</div>
	<div class="clear"></div>

	<div id="operations">
		<h1><?= ucwords($this->id) ?></h1>
		<?php $this->widget('zii.widgets.CMenu', [
			'items'=>$this->menu,
			'htmlOptions'=>['class'=>'menu'],
		]) ?>
	</div>
		
</header><!-- header -->

<?php if (Yii::app()->user->isGuest): ?>
	<?php link_to(t('admin', 'Login'), ['/site/login']) ?>
<?php else: ?>
	<?= link_to(t('admin', 'Logout'), ['/admin/default/logout']) ?>
<?php endif ?>

<div class="container">
	<?= $content ?>
</div><!-- /#container -->

<footer id="site-footer">
	<div class="container">
		Copyright &copy; <?= date('Y') ?> <?= params('appname') ?> All Rights Reserved.
	</div>
</footer><!-- footer -->

</body>
</html>