<?php

class AdminModule extends CWebModule {

	private $assetsUrl;

	public function getAssetsUrl()
	{
		if ($this->assetsUrl === null)
			$this->assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('admin.assets'));
		return $this->assetsUrl;
	}

	public function init()
	{
		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
			'application.models.*',
			'application.components.*',
		));
		
		Yii::app()->setComponents(array(
			'errorHandler'=>array(
				'class'=>'CErrorHandler',
				'errorAction'=>'admin/default/error',
			),
			'user'=>array(
				'class'=>'CWebUser',
				'stateKeyPrefix'=>'admin',
				'loginUrl'=>Yii::app()->createUrl('admin/default/login')
			)			
		), false);
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action))
		{
			$controller->layout = '/layouts/column1';
			
			$route = $controller->id.'/'.$action->id;

			$publicPages = array(
				'default/login',
				'default/error',
			);
			if (Yii::app()->user->isGuest && !in_array($route, $publicPages))
				Yii::app()->user->loginRequired();
			else
				return true;
		} else {
			return false;
		}
	}

}