<?php

class FilterWidget extends CWidget {
    
    public $view;

	public function run()
    {
		//set box title
		$title = Yii::t('common_v2', 'filter');
		
		//check if there is filter
		if(isset($this->view->filters) && !empty($this->view->filters)){
			$this->render('filter_box_widget_tpl', array('title' => $title, 'filters' => $this->view->filters));
		}
    }

}