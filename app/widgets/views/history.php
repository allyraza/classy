<div id="recently-viewed">
	<h3><?= t('common_v2', 'last viewed') ?></h3>

    <?php foreach($ads as $ad): ?>
    	<?= CHtml::link($ad->title, $ad->url, ['title' => $ad->title]) ?>
    <?php endforeach ?>
</div>
<!-- /#recently-viewed -->