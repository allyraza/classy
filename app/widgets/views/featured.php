<div id="featured-ads">
    
    <h1><?= t('homepage', 'Latest Classifieds') ?></h1>

    <?php foreach ($ads as $ad): ?>
    <article class="ad">
        <div class="thumb">
        	<a href="<?= $ad->viewUrl ?>" title="<?= e($ad->title) ?>">
        		<img src="<?= $ad->thumb ?>" width="120" height="90" alt="<?= e($ad->title) ?>" />
        	</a>
        </div>
        <div class="description">
            <a href="<?= $ad->viewUrl ?>" title="<?= e($ad->title) ?>">
            	<?= e($ad->title) ?>
            </a>
            <p><?= trunc(e($ad->description)) ?></p>
            <p class="info">
            	<?= t('common', 'Location') ?> : 
            	<b><?= $ad->location->name ?></b> | 
            	<?= t('common', 'Category') ?> : <b><?= $ad->category->name ?></b> | 
            	<?= t('common', 'Publish date') ?> : <b><?= $ad->publish_date ?></b>
            </p>
        </div>
    </article>
<?php endforeach ?>
</div>
<!-- /#featrued-ads -->