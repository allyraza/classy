<div class="categories">
	<ul>
	<?php foreach ($categories as $category): ?>
		<li>
			<?= link_to($category->name, $category->url, ['class' => 'heading']) ?>

		<?php if (!empty($category->children) && !$this->top): ?>
			<div class="sub-categories">
			<?php foreach ($category->children as $cat): ?>
				<?= link_to($cat->name, $cat->url) ?>
			<?php endforeach ?>
			</div>
		<?php endif ?>
	<?php endforeach ?>
	</ul>
</div>