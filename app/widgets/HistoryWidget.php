<?php

class HistoryWidget extends CWidget {

    public function run()
    {
    	if (!empty(Yii::app()->session['history']))
    	{
			$this->render('history', [
				'history' => array_reverse(Yii::app()->session['history'])
			]);
    	}
    }

}