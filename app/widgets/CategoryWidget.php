<?php

class CategoryWidget extends CWidget {

	public $top = false;

    public function run()
    {
    	$categories = Category::model()->toplevel()->findAll();
		$this->render('category', compact('categories'));
    }

}