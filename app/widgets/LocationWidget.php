<?php

class LocationWidget extends CWidget {

    public function run()
    {
		if (!empty(Yii::app()->session['lid']))
		{
			if (!$location = Yii::app()->cache->get( 'location_widget' ))
			{
				$location = Location::model()->findByPk(Yii::app()->session['lid']);
				Yii::app()->cache->set('location_widget' , $location);
			}
			printf('<strong>%s</strong><a href="%s">(%s)</a>', $location->name, url('ad/location'), t('common', 'clear'));
		}
    }

}