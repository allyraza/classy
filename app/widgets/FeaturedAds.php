<?php

class FeaturedAds extends CWidget {

	public $limit = 10;

	public function run()
	{
		$criteria=new CDbCriteria;
		if (!empty(Yii::app()->session['lid']))
			$criteria->compare('t.location_id', Yii::app()->session['lid']);

		$criteria->order = 't.id DESC';
		$criteria->limit = $this->limit;

		$cacheID = 'homepage.ads.';
		if (!$ads = Yii::app()->cache->get($cacheID))
		{
			$ads = Ad::model()->with('location', 'category')->findAll($criteria);
			Yii::app()->cache->set($cacheID , $ads);
		}
		
		$this->render('featured', compact('ads'));
	}

}