<?php

class PageMenuWidget extends CWidget {

	public function run()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('status', Page::ACTIVE);
		$criteria->order = 'sort_order ASC';
		$pages = Page::model()->findAll($criteria);
		
		$html = '';
		foreach ($pages as $page)
			$html .= sprintf('<li><a href="%s" title="%s">%s</a></li>', url('site/page', ['title' => $page->title]), $page->title, $page->title);
		echo $html;
	}

}