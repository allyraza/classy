<?php

class LocationBoxWidget extends CWidget {

	public function run()
    {

		$title = t('common', 'Locations');
		
    	$condition = 'status = 1 AND parent_id > 0';
		$params = array();
    	if (!empty(Yii::app()->session['lid']))
    	{
			$condition = 'status = 1 AND parent_id = :lid';
			$params[':lid'] = Yii::app()->session['lid'];
			$title = Yii::t('common_v2', 'cities');
		}
    	$locations = Location::model()->findAll($condition, $params);

    	$store = [];
		foreach ($locations as $location)
			$store[] = CHtml::link($location->name, url($location->slug), ['title' => $location->name]);
		
		$this->render('location', [
			'title' => $title, 
			'locations' => $store
		]);
    }

}