<?php

class CInit extends CComponent {

	public function init()
	{
		$params = Setting::model()->autoload();
		foreach ($params as $param)
			Yii::app()->params[$param->name] = $param->value;

		Yii::app()->name = Yii::app()->params['name'];
		Yii::app()->language = Yii::app()->params['lang'];
		Yii::app()->theme = Yii::app()->params['theme'];
	}

}