<?php

class ContactMailer extends CApplicationComponent {

	public function send($model)
	{
		$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
		$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
		$headers="From: $name <{$model->email}>\r\n".
			"Reply-To: {$model->email}\r\n".
			"MIME-Version: 1.0\r\n".
			"Content-type: text/plain; charset=UTF-8";
		if (mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers))
			$message = 'Thank you for contacting us. We will respond to you as soon as possible.';
		else
			$message = 'Error we could not send the email out. please try again.';

		Yii::app()->user->setFlash('contact', $message);
	}

}