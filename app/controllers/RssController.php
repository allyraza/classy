<?php

class RssController extends Controller {

	public function actionIndex()
	{
		$xml  = '<?xml version="1.0"?>
			<rss version="2.0">
				<channel>
					<title>' . t('home_page', 'pageTitle') . '</title>
						<description>' . t('home_page', 'pageDescription') . '</description>
						<link>' . siteUrl('/') . '</link>';

		$ads = Ad::model()->findAll(['limit' => 10, 'order' => 'id DESC']);

		foreach ($ads as $ad)
		{
			$link = url('ads/view', ['title' => $ad->slug]);
			$xml .= '<item>';
			$xml .= '<title>' . e($ad['ad_title']) . '</title>';
			$xml .= '<link>' . $link . '</link>';
			$xml .= '<description>' . e($ad['adescription']) . '</description>';
			$xml .= '<guid>' . $link . '</guid>';
			$xml .= '</item>';
		}
		$xml .= '</channel>
			</rss>';
		
		echo $xml;
		Yii::app()->end();
	}

}