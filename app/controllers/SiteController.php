<?php

class SiteController extends Controller {

	// @TODO: move it to database
	const HOMEPAGE_ADS_LIMIT = 10;

	/**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }
	
	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if ($error=Yii::app()->errorHandler->error)
		{
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	public function actionContact()
	{
		$model=new ContactForm;
		if (isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if ($model->validate())
			{
				ContactMailer::send($model);
				$this->refresh();
			}
		}
		$this->render('contact', compact('model'));
	}
	
	public function actionPage($title='')
	{
		$page = Page::model()->findByAttributes(array(
			'slug' => $title
		));

		if (empty($page))
			throw new CHttpException(404, "page not found.");
			
		$this->render('page', compact('page'));
	}

}