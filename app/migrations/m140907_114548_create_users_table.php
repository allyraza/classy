<?php

class m140907_114548_create_users_table extends CDbMigration {

	public function up()
	{
		$this->createTable("users", [
			"username" => "VARCHAR(64) NOT NULL",
			"first_name" => "VARCHAR(64) NOT NULL",
			"last_name" => "VARCHAR(64) NOT NULL",
			"password" => "VARCHAR(128) NOT NULL",
			"image" => "VARCHAR(128) NOT NULL",
			"token" => "VARCHAR(64) NOT NULL",
			"email" => "VARCHAR(64) NOT NULL",
			"phone" => "VARCHAR(32)",
			"role" => "TINYINT(1) DEFAULT 0",
			"status" => "TINYINT(1) DEFAULT 0",
			"last_login" => "DATETIME DEFAULT '0000-00-00 00:00:00'",
			"created_at" => "DATETIME DEFAULT '0000-00-00 00:00:00'",
			"updated_at" => "DATETIME DEFAULT '0000-00-00 00:00:00'",
		]);
		$this->createIndex("users_username", "users", "username");
		$this->createIndex("users_token", "users", "token");
	}

	public function down()
	{
		$this->dropTable("users");
	}

}