<?php

class m140907_115417_create_pages_table extends CDbMigration {

	public function up()
	{
		$this->createTable('pages', [
			"id" => "pk",
			"title" => "VARCHAR(255) NOT NULL",
			"slug" => "VARCHAR(255)",
			"content" => "LONGTEXT",
			"meta_title" => "VARCHAR(255)",
			"meta_description" => "VARCHAR(255)",
			"meta_keywords" => "VARCHAR(255)",
			"status" => "TINYINT(1) DEFAULT 1",
			"created_at" => "DATETIME DEFAULT '0000-00-00 00:00:00'",
			"updated_at" => "DATETIME DEFAULT '0000-00-00 00:00:00'",
		]);
		$this->createIndex("pages_slug", "pages", "slug");
	}

	public function down()
	{
		$this->dropTable("pages");
	}

}