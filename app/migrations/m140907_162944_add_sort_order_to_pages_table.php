<?php

class m140907_162944_add_sort_order_to_pages_table extends CDbMigration {

	public function up()
	{
		$this->addColumn("pages", "sort_order", "INT(11) DEFAULT 0 AFTER meta_keywords");
	}

	public function down()
	{
		$this->dropColumn("pages", "sort_order");
	}

}