<?php

class m140907_115957_create_images_table extends CDbMigration {

	public function up()
	{
		$this->createTable('images', [
			"id" => "pk",
			"imageable_id" => "INT(11) NOT NULL DEFAULT 0",
			"imageable_type" => "VARCHAR(32) NOT NULL",
			"name" => "VARCHAR(255) NOT NULL",
			"created_at" => "DATETIME DEFAULT '0000-00-00 00:00:00'",
			"updated_at" => "DATETIME DEFAULT '0000-00-00 00:00:00'",
		]);

		$this->createIndex("images_imageable_id", "images", "imageable_id");
		$this->createIndex("images_imageable_type", "images", "imageable_type");
	}

	public function down()
	{
		$this->dropTable('images');
	}

}