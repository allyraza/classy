<?php

class m140907_115309_create_categories_table extends CDbMigration {

	public function up()
	{
		$this->createTable("categories", [
			"id" => "pk",
			"name" => "VARCHAR(128) NOT NULL",
			"slug" => "VARCHAR(128)",
			"parent_id" => "INT(11) DEFAULT 0",
			"status" => "TINYINT(1) DEFAULT 1",
			"created_at" => "DATETIME DEFAULT '0000-00-00 00:00:00'",
			"updated_at" => "DATETIME DEFAULT '0000-00-00 00:00:00'",
		]);
		$this->createIndex("categories_parent", "categories", "parent_id");
		$this->createIndex("categories_slug", "categories", "slug");
	}

	public function down()
	{
		$this->dropTable("categories");
	}

}