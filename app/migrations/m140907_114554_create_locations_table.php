<?php

class m140907_114554_create_locations_table extends CDbMigration {

	public function up()
	{
		$this->createTable("locations", [
			"id" => "pk",
			"name" => "VARCHAR(128) NOT NULL",
			"slug" => "VARCHAR(128)",
			"parent_id" => "INT(11) DEFAULT 0",
			"type" => "VARCHAR(16) NOT NULL",
			"status" => "TINYINT(1) DEFAULT 1",
			"created_at" => "DATETIME DEFAULT '0000-00-00 00:00:00'",
			"updated_at" => "DATETIME DEFAULT '0000-00-00 00:00:00'",
		]);
		$this->createIndex("locations_parent", "locations", "parent_id");
		$this->createIndex("locations_type", "locations", "type");
		$this->createIndex("locations_slug", "locations", "slug");
	}

	public function down()
	{
		$this->dropTable("locations");
	}

}