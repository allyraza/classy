<?php

class m140907_160602_add_sort_order_to_categories_table extends CDbMigration {

	public function up()
	{
		$this->addColumn("categories", "sort_order", "INT(11) DEFAULT 0 AFTER parent_id");
	}

	public function down()
	{
		$this->dropColumn("categories", "sort_order");
	}

}