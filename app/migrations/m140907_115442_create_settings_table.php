<?php

class m140907_115442_create_settings_table extends CDbMigration {

	public function up()
	{
		
		$this->createTable("settings", [
			"id" => "pk",
			"name" => "VARCHAR(64) NOT NULL",
			"value" => "TEXT",
			"group" => "VARCHAR(32)",
			"autoload" => "TINYINT(1) DEFAULT 0"
		]);

		$this->createIndex("settings_name", "settings", "name");
		$this->createIndex("settings_group", "settings", "group");

		Yii::import("application.models.Setting");
		$this->insert("settings", [
			"name" => "theme",
			"value" => "basic",
			"autoload" => Setting::AUTOLOAD
		]);

		$this->insert("settings", [
			"name" => "name",
			"value" => "Classy",
			"autoload" => Setting::AUTOLOAD
		]);

		$this->insert("settings", [
			"name" => "lang",
			"value" => "en",
			"autoload" => Setting::AUTOLOAD
		]);

	}

	public function down()
	{
		$this->dropTable('settings');
	}

}