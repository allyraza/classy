<?php

class m140906_100749_create_ads_table extends CDbMigration {

	public function up()
	{
		$this->createTable('ads', [
			"id" => "pk",
			"user_id" => "TINYINT(2) DEFAULT 0",
			"category_id" => "INT(11) DEFAULT 0",
			"location_id" => "INT(11) DEFAULT 0",
			"title" => "VARCHAR(255) NOT NULL",
			"price" => "DOUBLE(10,2) DEFAULT 0.00",
			"currency_id" => "TINYINT(2) DEFAULT 1",
			"description" => "LONGTEXT",
			"description_filtered" => "LONGTEXT",
			"type" => "VARCHAR(32)",
			"views" => "INT(11) DEFAULT 0",
			"highlight" => "TINYINT(1) DEFAULT 0",
			"display_phone" => "TINYINT(1) DEFAULT 1",
			"display_email" => "TINYINT(1) DEFAULT 1",
			"status" => "TINYINT(1) DEFAULT 0",
			"expiry_date" => "DATETIME DEFAULT '0000-00-00 00:00:00'",
			"created_at" => "DATETIME DEFAULT '0000-00-00 00:00:00'",
			"updated_at" => "DATETIME DEFAULT '0000-00-00 00:00:00'",
		]);
		$this->createIndex("ads_user", "ads", "user_id");
		$this->createIndex("ads_category", "ads", "category_id");
		$this->createIndex("ads_location", "ads", "location_id");
		$this->createIndex("ads_status", "ads", "status");
		$this->createIndex("ads_highlight", "ads", "highlight");
		$this->createIndex("ads_type", "ads", "type");
		$this->createIndex("ads_created_at", "ads", "created_at");
		$this->createIndex("ads_expiry_date", "ads", "expiry_date");
	}

	public function down()
	{
		$this->dropTable('ads');
	}

}