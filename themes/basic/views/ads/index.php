<?php

$this->view->title = '';
$this->view->description = '';
$this->view->keywords = '';

?>

<h1><?= $summary ?></h1>
<?php $this->widget('zii.widgets.CListView', [
	'dataProvider' => $dataProvider,
	'itemView' => '_view'
]) ?>
