<?php
$this->view->title = t('homepage', 'title');
$this->view->description = t('homepage', 'description');
$this->view->keywords = t('homepage', 'keywords');
?>

<?php if ($this->beginCache('homepage.categories')) { ?>
<?php $this->widget('application.widgets.CategoryWidget') ?>
<?php $this->endCache(); }?>
