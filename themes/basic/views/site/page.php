<?php
$this->view->title = $page->title or '';
$this->view->description = $page->meta_description or '';
$this->view->keywords = $page->meta_keywords or '';
?>
<div class="page-container">
	<?= e($page->content) ?>
</div>