<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);


$this->view->title = 'Page - ';
$this->view->description = '';
$this->view->keywords = '';
?>

<h2>Error <?= $code ?></h2>

<div class="error">
<?= e($message) ?>
</div>