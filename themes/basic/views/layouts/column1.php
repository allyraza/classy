<?php $this->beginContent("//layouts/main") ?>
<div class="container">

	<div id="content">
		<?= $content ?>
	</div>
	<!-- /#content -->

	<div id="sidebar">
		
	</div>

</div>
<!-- /.container -->
<?php $this->endContent() ?>