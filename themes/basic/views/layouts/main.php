<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?= e($this->view->title) ?></title>
	
	<!-- META -->
	<meta name="description" content="<?= e($this->view->description)?>">
	<meta name="keywords" content="<?= e($this->view->keywords)?>">

	<meta name="revisit-after" content="1 Days">
	<meta name="robots" content="index,follow">
	<meta name="googlebot" content="index,follow">
	<meta name="rating" content="general">

	<?php if (!empty($this->view->ad->image)): ?>
	<meta property="og:title" content="<?= e($this->view->ad->title) ?>"/>
	<meta property="og:site_name" content="<?= 'foobar.com' ?>"/> 
	<meta property="og:image" content="<?= upload('ads/small-' . $this->view->ad->image) ?>"/>
	<?php endif ?>

	<base href="<?= site_url('/') ?>" />

	<!-- STYLES -->
	<!-- <link rel="stylesheet" type="text/css" href="<?= asset('css/main.css') ?>" media="screen, projection" /> -->
<?php 

Yii::app()->clientScript
	->registerCoreScript('jquery')
	->registerScriptFile(asset('js/main.js'))
	->registerCssFile(asset('css/main.css'));

?>

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link rel="alternate"  type="application/rss+xml" title="<?= t('home_page', 'Latest Classifieds')?>" href="<?= site_url('/') ?>/rss" />

</head>
<body class="page <?= $this->id ?>">

	<header id="site-header">
		<div class="container">
		
			<div id="logo">
				<a href="<?= Yii::app()->baseUrl ?>" title="<?= Yii::app()->name ?>">
					<?= Yii::app()->name ?>
				</a>
			</div>

			<div id="search">
				<form action="<?= url('ads/search') ?>" method="get">
					<input type="text" name="what" placeholder="Find a...">
					<input type="text" name="where" placeholder="e.g. town, city">
					<button type="submit"><?= t('common', 'Search') ?></button>
				</form>
			</div>

			<?php $this->widget('application.widgets.LocationWidget') ?>

			<nav id="mainmenu">
				<ul>
					<li class="dropdown-trigger">
						<a href="#"><?= t('common', 'Browse') ?></a>
						<?php if ($this->beginCache('browse.category.widget')) { ?>
						<?php $this->widget('application.widgets.CategoryWidget', ['top' => true]) ?>
						<?php $this->endCache(); } ?>
					</li>
					<li>
						<a href="<?= url('ads/publish') ?>"><?= t('common', 'Post an Ad') ?></a>
					</li>
					<li>
						<a href="<?= url('site/contact') ?>"><?= t('homepage', 'Login') ?></a>
					</li>
				</ul>
			</nav>
	
		</div>
	</header>
	<!-- /header -->

	<?= $content ?>

	<footer id="site-footer">
		<div class="container">

			<div style="float:left;">
				Copyright &copy; <?= date('Y') ?> <?= Yii::app()->name ?>
			</div>
			<div style="float:right;">
				Powered by <a href="#" title="free classifieds script" target="_blank"><?= Yii::app()->name ?></a>
			</div>
			<div class="clear"></div>

		</div>
	</footer>
	<!-- /#site-footer -->

</body>
</html>