<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<title><?= $this->view->title ?></title>
	
	<!-- META -->
	<meta name="description" content="<?= $this->view->description ?>">
	<meta name="keywords" content="<?= $this->view->keywords ?>">
	<meta name="robots" content="index,follow">
	<meta name="googlebot" content="noindex,nofollow">
	<meta name="rating" content="general">

	<base href="<?= site_url('/') ?>" />

	<!-- STYLES -->
	<link rel="stylesheet" type="text/css" href="<?= asset('css/main.css') ?>" media="screen, projection" />

<?php

Yii::app()->clientScript
	->registerScriptFile('http://maps.googleapis.com/maps/api/js?sensor=true&language=' . params('lang') , CClientScript::POS_END)
	->registerScriptFile(asset('/front/js/google.map.js'), CClientScript::POS_END);

?>

</head>
<body>

<?= $content ?>

</body>
</html>