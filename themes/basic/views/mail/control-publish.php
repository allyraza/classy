<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--
body,td,th {
	font-family: Tahoma;
	font-size: 12px;
	color: #000000;
}
body {
	background-color: #FFFFFF;
}
-->
</style>
</head>
<body>

<?php
$viewUrl = site_url('ads/view' , ['id' => $ad->id]);
$deleteUrl = site_url('ads/delete', ['id' => $ad->id]);
$editUrl = site_url('ads/edit', ['id' => $ad->id]);
?>

	IP: <?= $ad->ip ?>
	<br /><br />
	<?= t('publish_page', 'You can view your classified ad here') ?> : 
	<br /><br />
	<a href="<?= $viewUrl ?>"><?= $viewUrl ?></a>
	<br /><br />
	<b><?= $ad->title?></b>
	<br /><br />
	<?= $ad->description ?>
	<br /><br />

<h1><?= t('publish_page', 'Important!') ?></h1>
<?= t('publish_page_v2', 'You can edit your classified ad by clicking on the following link') ?> :
<a href="<?= $editUrl ?>"><?= $editUrl ?></a>
<br /><br />
<?= t('publish_page', 'and enter this code') ?> : <b><?= $code ?></b>

<br/><br/>

<?= t('publish_page', 'You can delete your classified ad by clicking on the following link')?> :
<a href="<?= $deleteUrl ?>"><?= $deleteUrl ?></a><br />
<?= t('publish_page', 'and enter this code') ?> : <b><?= $code ?></b>

</body>
</html>