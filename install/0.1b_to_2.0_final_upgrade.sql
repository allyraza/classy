CREATE TABLE IF NOT EXISTS `ad_ban_ip` (
  `ban_ip_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ban_ip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ban_ip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `ad_ban_email` (
  `ban_email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ban_email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ban_email_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `ad_type` (
  `ad_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ad_type_name` varchar(255) NOT NULL,
  PRIMARY KEY (`ad_type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

INSERT INTO `ad_type` (`ad_type_id`, `ad_type_name`) VALUES
(1, 'Sale'),
(2, 'Buy'),
(3, 'Gift');

CREATE TABLE IF NOT EXISTS `ad_valid` (
  `ad_valid_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ad_valid_days` int(11) DEFAULT NULL,
  `ad_valid_name` varchar(255) DEFAULT NULL,
  `ad_valid_ord` int(11) DEFAULT NULL,
  PRIMARY KEY (`ad_valid_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

INSERT INTO `ad_valid` (`ad_valid_id`, `ad_valid_days`, `ad_valid_name`, `ad_valid_ord`) VALUES
(1, 15, '15 Days', 1),
(2, 30, '30 Days', 2),
(3, 180, '6 Months', 3),
(4, 365, '1 Year', 4);

ALTER TABLE `ad` ADD `ad_type_id` INT UNSIGNED NOT NULL AFTER `ad_id`;
ALTER TABLE `ad` ADD `ad_link` VARCHAR( 255 ) NULL DEFAULT NULL;
ALTER TABLE `ad` ADD `ad_video` VARCHAR( 255 ) NULL DEFAULT NULL;
ALTER TABLE `ad` ADD `ad_lat` VARCHAR( 50 ) NULL DEFAULT NULL;
ALTER TABLE `ad` ADD `ad_lng` VARCHAR( 50 ) NULL DEFAULT NULL;
ALTER TABLE `ad` ADD `ad_skype` VARCHAR( 255 ) NULL DEFAULT NULL;
ALTER TABLE `ad` ADD `ad_valid_until` DATE NULL DEFAULT NULL;
ALTER TABLE `ad` ADD `ad_address` VARCHAR( 255 ) NULL DEFAULT NULL;
ALTER TABLE `ad` ADD `ad_valid_id` INT NULL DEFAULT NULL;
ALTER TABLE `ad` ADD `ad_pic` VARCHAR( 255 ) NULL DEFAULT NULL;
ALTER TABLE `ad` CHANGE `ad_price` `ad_price` DOUBLE( 10, 2 ) NULL DEFAULT NULL;
UPDATE ad SET ad_type_id = 1;

ALTER TABLE `category` ADD `category_description` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `category_title`;
ALTER TABLE `category` ADD `category_keywords` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `category_description`;
ALTER TABLE `location` ADD `location_parent_id` INT NULL DEFAULT NULL AFTER `location_id`;

INSERT INTO `settings` (`setting_id`, `setting_name`, `setting_value`, `setting_description`, `setting_show_in_admin`) VALUES
(17, 'ENABLE_VISUAL_EDITOR', '1', 'Enable Visual Editor', 1),
(18, 'ENABLE_GOOGLE_MAP', '1', 'Enable Google Map', 1),
(19, 'ENABLE_TIPSY_PUBLISH', '1', 'Enable Tips on Publish', 1),
(20, 'ENABLE_VIDEO_LINK_PUBLISH', '1', 'Enable video link publish', 1),
(21, 'ENABLE_LINK_PUBLISH', '1', 'Enable link publish', 1),
(22, 'MAX_PIC_UPLOAD_SIZE', '204800', 'Maximum upload image size', 1),
(23, 'NUM_PICS_UPLOAD', '3', 'Num upload pics publish', 1),
(24, 'NUM_HISTORY_ITEMS', '5', 'Num classifieds in history box', 1),
(25, 'NUM_ITEMS_IN_RSS', '100', 'Num items in rss feed', 1),
(26, 'PRICE_CURRENCY_NAME', '&euro;', 'Site currency name', 1);

ALTER TABLE `ad` ADD INDEX ( `category_id` );
ALTER TABLE `ad` ADD INDEX ( `location_id` );
ALTER TABLE `ad` ADD INDEX ( `ad_type_id` );
ALTER TABLE `ad_pic` ADD INDEX ( `ad_id` );

UPDATE ad SET ad_pic = (SELECT ad_pic_path FROM ad_pic AS AP WHERE AP.ad_id = ad.ad_id ORDER BY ad_pic_id LIMIT 1);
